import React from 'react'
import AwesomeSlider from 'react-awesome-slider'
import { Box, Container, Flex, Text } from 'theme-ui'
import { SlideContainer } from './common/SlideContainer'

import 'react-awesome-slider/dist/styles.css'
import '../styles/sliderStyles.css'

const Home = (): JSX.Element => {
  return (
    <Flex sx={{ justifyContent: 'flex-start' }}>
      <Box
        variant="noMargin"
        sx={{
          backgroundColor: '#5172E2',
          height: '615px',
          marginTop: '-100px',
          position: 'relative',
          width: '35%',
          zIndex: 2,
        }}
      >
        <Container
          sx={{
            bottom: 0,
            position: 'absolute',
            right: 0,
            width: '80%',
          }}
        >
          <Text
            sx={{
              color: 'white',
              textAlign: 'center',
              fontSize: '30px',
              fontWeight: 'bold',
            }}
          >
            SISTEMA DE CONTROL DE FARMACIAS <br /> “MI SALUD”
          </Text>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <img
            alt="Logo"
            src={'https://cdn-icons-png.flaticon.com/512/7390/7390467.png'}
            width="180"
          />
        </Container>
      </Box>
      <Box
        sx={{
          height: '715px',
          justifyContent: 'center',
          position: 'relative',
          width: '65%',
        }}
      >
        <Box
          sx={{
            alignItems: 'center',
            bottom: 200,
            height: '480px',
            left: 0,
            margin: 'auto',
            position: 'absolute',
            right: 0,
            top: 0,
            width: '790px',
          }}
        >
          <AwesomeSlider fillParent={true}>
            <Box>
              <SlideContainer
                image="https://www.ocu.org/-/media/ocu/images/home/salud/medicamentos/farmacias-atencion-deficiente.jpg?rev=b66335e7-4757-4cd1-ba15-edf4d68a8f0f&hash=7CA47EA0C13FF20E4044E3D5059DD002"
                imageName="slide1"
                slideTitle=""
                summary=""
              ></SlideContainer>
            </Box>
            <Box>
              <SlideContainer
                image="https://img.freepik.com/foto-gratis/vista-cerca-mano-farmaceutico-tomando-caja-medicina-estante-farmacia_342744-320.jpg?w=900&t=st=1663892107~exp=1663892707~hmac=74b2aa8fef2e33e03c2b52532416dadf61187f137e95d7c49efb84e352ef7389?w=200"
                imageName="slide1"
                slideTitle=""
                summary=""
              ></SlideContainer>
            </Box>
            <Box>
              <SlideContainer
                image="https://img.freepik.com/foto-gratis/farmaceutico-mujer-trabajando-farmacia_259150-57972.jpg?w=996&t=st=1663893597~exp=1663894197~hmac=5244dd42724bb1f3e621330048b478f849b0a7c9c52ae2d1fb48fddf54dcf1b2"
                imageName="slide1"
                slideTitle=""
                summary=""
              ></SlideContainer>
            </Box>
          </AwesomeSlider>
        </Box>
      </Box>
    </Flex>
  )
}

export default Home
