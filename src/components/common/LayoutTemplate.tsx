/** @jsxRuntime classic */
/** @jsx jsx */

import { jsx } from 'theme-ui'

import Footer from '../Footer'
import Header from '../Header'

interface propTypes {
  children: React.ReactNode
  className?: string
}

function LayoutTemplate(props: propTypes): JSX.Element {
  return (
    <div>
      <div
        sx={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: '100vh',
        }}
      >
        <Header />
        <main
          sx={{
            flex: '1 1 auto',
            margin: 0,
            padding: 0,
            width: '100%',
          }}
        >
          {props.children}
        </main>
        <div
          sx={{
            marginTop: 0,
          }}
        >
          <h4>Integrantes:</h4>
          <ul>
            <li>Abel William Copa</li>
            <li>Alvaro Diego Daza Alcaraz</li>
            <li>Enzo Aranibar</li>
            <li>Freddy Aguilar Rojas</li>
            <li>Johnny Arteaga Mendoza</li>
            <li>Lisbeth Quisbert Patzi</li>
            <li>Y. Jazmin Cussi Callisaya</li>
          </ul>
        </div>
        <Footer />
      </div>
    </div>
  )
}

export default LayoutTemplate
