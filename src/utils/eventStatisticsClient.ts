import { GeneralEventStatistics } from './../redux/types'

const routes: string | undefined = process.env.REACT_APP_API

export async function getAllEventsCount(): Promise<GeneralEventStatistics> {
  try {
    const response = await fetch(`https://gateway-events.azurewebsites.net/v1.0/eventStatistics/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    return await response.json()
  } catch (error) {
    throw new Error(error.toString())
  }
}
